**This is a parallel GPU AQS shear code with fire minimization for Kob Anderson model, written in CUDA.**
   
 * I have added a comment with capital letters "TWEAK THIS" ahead of those variables which you can tweak and see the results
 	 
 * You must have the nvcc compiler installed.

 * To install nvcc compiler in linux, first install recommended nvidia driver and then install cuda with the folloing command
    

 *      sudo apt install nvidia-cuda-toolkit

 *	 Compile code using: 
 *	 	 nvcc md_gpu_AQS_ShearFIRE.cu -arch=sm_61 -lm -Xptxas -O3,-v --use_fast_math -w

      
      -arch=sm_61 is the flag used for the GPU card architecture. -arch=sm_61
     is used for Geforce GTX 1050 Ti (my machine’s GPU). To run above codes on
     Tesla V100, change -arch=sm_61 to -arch=sm_70.





  Read input Data:<br/><br/>

  Coordinate file   :: 		positionLAMMPSHimangsu.dat (N=4000, number density = 1.2, T = 1)<br/>
  Format:<br/><pre>
    Particle Id		type	x	y	z	mx	my	mz
    ....</pre>

    
![AQSKAPE](/uploads/13f336c70f53e1f537eb9eb9972f182c/AQSKAPE.png)

![AQSKAPEAll](/uploads/d15cf7d1bc325079cff330350e9b4535/AQSKAPEAll.png) 
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020

